import { Component, OnInit, ViewChild } from '@angular/core';

import { CustomisedEditButtonComponent } from '../customised-edit-button/customised-edit-button.component';

import { GridDataService } from 'src/app/shared/grid-data.service';

import { HttpClient } from '@angular/common/http';

// import { AllCommunityModules, Module } from "@ag-grid-community/all-modules";

import { GridOptions } from 'ag-grid-community';

@Component({

  selector: 'app-customer-table',

  templateUrl: './customer-table.component.html',

  styleUrls: ['./customer-table.component.css']

})

export class CustomerTableComponent implements OnInit {

  frameworkComponents: any;

  rowDataClicked1 = {};

  rowData: any;

  rowDataClicked2 = {};

  private gridApi

   columnDefs

  private gridColumnApi

   rowSelection

   gridOptions: GridOptions;

  // public modules: Module[] = AllCommunityModules;

  constructor(private griddataservice: GridDataService, private httpservice: HttpClient) {

    this.gridOptions = {

      suppressCellSelection: true

    };

    this.columnDefs = [

      { headerName: 'Name', field: 'name' },

      { headerName: 'Email', field: 'email' },

      { headerName: 'Phone', field: 'phone' },

      { headerName: 'Age', field: 'age' },

      { headerName: 'City', field: 'city' },

      { headerName: 'Pincode', field: 'pincode' },

      {

        cellRenderer: "customisedEdit", field: 'three',

        suppressNavigable: true,

        editable: false,

        cellClass: 'no-border'

        // cellRendererParams: {

        //   onClick: this.onBtnClick1.bind(this),

        //   label: 'Click 1'

      },

      // },  {

      //   cellRenderer: "customisedEdit",

      // cellRendererParams: {

      //   onClick: this.onBtnClick1.bind(this),

      //   label: 'Click 1'

      // }

      //  },

      this.frameworkComponents = {

        customisedEdit: CustomisedEditButtonComponent

      },

      this.rowSelection = "single",

    ];

  }

  onRowClick(event: any): void {

    console.log(event.rowIndex);

  }

  ngOnInit() {

    this.httpservice.get('../assets/griddata.json').subscribe(

      data => {

        this.rowData = data;               // FILL THE ARRAY WITH DATA.

      },

    )

 

  }

  onGridReady(params) {

    this.gridApi = params.api;

    this.gridColumnApi = params.columnApi;

  }

 

  singleRowData: any

  // getRow() {

  //   this.singleRowData = this.gridApi.getSelectedRows()

  //   console.log(this.gridApi.getSelectedRows()[0].name);

  // }

  getRow() {

    var selectedData = this.gridApi.getSelectedRows();

    var res = this.gridApi.updateRowData({ remove: selectedData });

  }

 

  onBtnClick1(e) {

    console.log(e.rowData.age);

    this.rowDataClicked1 = e.rowData;

  }

 

}

 
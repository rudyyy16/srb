import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
@Component({
  selector: 'app-customer-details',
  templateUrl: './customer-details.component.html',
  styleUrls: ['./customer-details.component.css']
})
export class CustomerDetailsComponent implements OnInit {

 
  constructor(private fb: FormBuilder) { }
  userRegForm = this.fb.group({
    name: ['rob'],
    phone: [''],
    email: [''],
    age: [''],
    address: this.fb.group({
      city: ['asa'],
      pincode: ['']
    })
  })
  

  ngOnInit() {
  }

}

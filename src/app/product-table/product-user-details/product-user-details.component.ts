import { Component, OnInit, ViewChild } from '@angular/core';

import { ProductEditButtonComponent } from '../product-edit-button/product-edit-button.component';

import { GridDataService } from 'src/app/shared/grid-data.service';

import { HttpClient } from '@angular/common/http';

// import { AllCommunityModules, Module } from "@ag-grid-community/all-modules";

 

@Component({

  selector: 'app-product-user-details',

  templateUrl: './product-user-details.component.html',

  styleUrls: ['./product-user-details.component.css']

})

export class ProductUserDetailsComponent implements OnInit {

  frameworkComponents: any;

  rowDataClicked1 = {};

  rowData: any;

  rowDataClicked2 = {};

   gridApi

   columnDefs

   gridColumnApi

   rowSelection

  // public modules: Module[] = AllCommunityModules;

  constructor(private httpservice: HttpClient) {

    this.columnDefs = [

      { headerName: 'Customer ID', field: 'customer_id' },

      { headerName: 'Customer Name', field: 'customer_name' },

      { headerName: 'Customer Phone Number', field: 'customer_number' },

      this.rowSelection = "single"

    ];

  }

  onRowClick(event: any): void {

    console.log(event.rowIndex);

  }

  ngOnInit() {

    this.httpservice.get('../assets/product-users.json').subscribe(

      data => {

        this.rowData = data;               // FILL THE ARRAY WITH DATA.

      },

    )

 

  }

  onGridReady(params) {

    this.gridApi = params.api;

    this.gridColumnApi = params.columnApi;

  }

 

  singleRowData: any

  // getRow() {

  //   this.singleRowData = this.gridApi.getSelectedRows()

  //   console.log(this.gridApi.getSelectedRows()[0].name);

  // }

  getRow() {

    var selectedData = this.gridApi.getSelectedRows();

    var res = this.gridApi.updateRowData({ remove: selectedData });

  }

 

  onBtnClick1(e) {

    console.log(e.rowData.age);

    this.rowDataClicked1 = e.rowData;

  }

 

}

 

 
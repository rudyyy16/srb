import { Component, OnInit, ViewChild } from '@angular/core';

import { ProductEditButtonComponent } from '../product-edit-button/product-edit-button.component';

import { GridDataService } from 'src/app/shared/grid-data.service';

import { HttpClient } from '@angular/common/http';

// import { AllCommunityModules, Module } from "@ag-grid-community/all-modules";

import { ProductUserDetailsComponent } from '../product-user-details/product-user-details.component';

@Component({

  selector: 'app-product-table',

  templateUrl: './product-table.component.html',

  styleUrls: ['./product-table.component.css']

})

export class ProductTableComponent implements OnInit {

  frameworkComponents: any;

  rowDataClicked1 = {};

  rowData: any;

  rowDataClicked2 = {};

   gridApi

   columnDefs

   gridColumnApi

   getRowNodeId;

   rowSelection

  // public modules: Module[] = AllCommunityModules;

  constructor( private httpservice: HttpClient) {

    this.columnDefs = [

      { headerName: 'Product ID', field: 'product_id' },

      { headerName: 'Product Name', field: 'product_name' },

      { headerName: 'Product Price', field: 'product_price' },

      { headerName: 'Product Quantity', field: 'product_quantity' },

      { headerName: 'Product Users', field: 'product_users' },

      { headerName: 'Pincode', field: 'product_pincode' },

      {

         cellRenderer: "productEdit",

        // cellRendererParams: {

        //   onClick: this.onBtnClick1.bind(this),

        //   label: 'Click 1'

        // }

      },  {

        cellRenderer: "productEdit",

       // cellRendererParams: {

       //   onClick: this.onBtnClick1.bind(this),

       //   label: 'Click 1'

       // }

     },

      this.frameworkComponents = {

        productEdit: ProductEditButtonComponent

      },

      // this.getRowNodeId = function(data) {

      //   return data.id;

      // },

      this.rowSelection = "single"

    ];

  }

onRowClick(event: any): void {

      console.log(event.rowIndex);

    }

  ngOnInit() {

    this.httpservice.get('../assets/product.json').subscribe(

      data => {

        this.rowData = data;               // FILL THE ARRAY WITH DATA.

      },

    )

 

  }

  onGridReady(params) {

    this.gridApi = params.api;

    this.gridColumnApi = params.columnApi;

   

  }

 

  singleRowData: any

  // getRow() {

  //   this.singleRowData = this.gridApi.getSelectedRows()

  //   console.log(this.gridApi.getSelectedRows()[0].name);

  // }

  getRow() {

    var selectedData = this.gridApi.getSelectedRows();

    var res = this.gridApi.updateRowData({ remove: selectedData });

  }

 

  onBtnClick1(e) {

    console.log(e.rowData.age);

    this.rowDataClicked1 = e.rowData;

  }

 

}

 

